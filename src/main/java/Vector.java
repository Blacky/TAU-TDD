import java.util.List;

interface Vector {
    VectorImpl add(Vector v1);
    List<Double> getValues();
}
