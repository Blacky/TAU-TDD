import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class VectorImpl implements Vector{

    public List<Double> getValues() {
        return values;
    }

    public void setValues(List<Double> values) {
        this.values = values;
    }

    private List<Double> values = new ArrayList<>();

    VectorImpl(){};

    VectorImpl(List<Double> values){
        setValues(values);
    }

    @Override
    public VectorImpl add(Vector v1) {
        VectorImpl vector = new VectorImpl();
        List<Double> values = new ArrayList<>();
        for (Double v: v1.getValues()) {
            values.add(v+v);
        }
        vector.setValues(values);
        return vector;
    }

    static public Vector add(Vector v1, Vector v2) {
        VectorImpl vector = new VectorImpl();
        List<Double> values = new ArrayList<>();

        Iterator<Double> iterator1 = v1.getValues().iterator();
        Iterator<Double> iterator2 = v2.getValues().iterator();

        while(iterator1.hasNext() && iterator2.hasNext()){
            values.add(iterator1.next()+iterator2.next());
        }

        vector.setValues(values);
        return vector;
    }
}
