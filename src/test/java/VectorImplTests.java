import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class VectorImplTests {

    @Test
    public void addingPositiveAndNegativenumbersVectorsTest() {
        List<Double> list1 = new ArrayList<>();
        List<Double> list2 = new ArrayList<>();
        List<Double> checkList = new ArrayList<>();
        list1.add(2.2);
        list1.add(2.3);
        list2.add(4.3);
        list2.add(5.4);
        checkList.add(6.5);
        checkList.add(7.7);
        VectorImpl vector1 = new VectorImpl(list1);
        VectorImpl vector2 = new VectorImpl(list2);
        Vector vectorResult = VectorImpl.add(vector1,vector2);
        assertEquals(vectorResult.getValues().get(0),checkList.get(0));
        assertEquals(vectorResult.getValues().get(1),checkList.get(1));
    }
}
